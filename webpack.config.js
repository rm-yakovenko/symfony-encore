var Encore = require('@symfony/webpack-encore');

Encore
// directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // will output as web/build/app.js
    .addEntry('home', './assets/js/home.js')
    .addEntry('homeCarousel', './assets/js/homeCarousel.js')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    .createSharedEntry('vendor', [
        'jquery',
        'bootstrap',
        'owl.carousel/dist/owl.carousel.min',
        'owl.carousel/dist/assets/owl.carousel.css',
        'select2/dist/css/select2.css',
    ])

// create hashed filenames (e.g. app.abc123.css)
// .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();